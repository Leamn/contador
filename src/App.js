import './App.css';
import Button from './components/button';
import Contador from './components/contador';

function App() {
  return (
    <div className="App">
      <div>
        <img src="https://serene-yalow-4d737f.netlify.app/static/media/RedondaAzul.1d9cb401.ico"></img>
        <h1>ContAcens, o contador da Acens!</h1>
        <Contador />
        <Button />
      </div>
    </div>
  );
}

export default App;
