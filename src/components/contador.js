import { useState } from 'react';


function Contador(props) {
  const [contador, setContador] = useState(0);
  

  return (
    <div className="Contador">
       Valor: {contador}
    </div>
  );
}

export default Contador;