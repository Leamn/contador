import { useState } from 'react';
import Contador from './contador';


function Button(props) {
 
  const [contador, setContador] = useState(0);
    function Add() {
        setContador(contador + 1);
    }
    function Remove() {
        setContador(contador - 1);
    }
    function Reset() {
        setContador(0);
    }
    <Contador value={contador} />
    console.log(contador);
    console.log(props.value);
    
  return (
    <div className="Button">
      <div>Valor: {contador}</div>
      <button id="add" onClick={Add}>Adicionar</button>
      <button id="remove" onClick={Remove}>Remover</button>
      <button id="reset" onClick={Reset}>Resetar</button>
    </div>
  );
}

export default Button;
